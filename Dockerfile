FROM openjdk:11-jre-slim
RUN apt-get update
RUN apt-get -y install git
RUN apt-get -y install python3

RUN mkdir -p /home/user/.ssh/
RUN mkdir -p /home/user/repository/
RUN mkdir -p /usr/local/bin/
RUN mkdir -p /usr/app/

RUN chmod g+w /etc/passwd
RUN chmod -R g+rwX /home/user

COPY ./build/libs/address-provider-0.0.1-SNAPSHOT.jar /usr/app/
COPY ./entrypoint.py /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.py

WORKDIR /usr/app
ENTRYPOINT /usr/local/bin/entrypoint.py
