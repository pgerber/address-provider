# Address Provider

## Getting started

**Build:**
```
./gradlew build
```

**Create empty PostgreSQL database named `address_provider`:**

Manually execute the script `src/main/resources/schema.sql`

If you create a pull request with changes the database schema you must manually execute the database script at VSHN!

**Add a local config file:**

Config file loaction: `src/main/resources/application-dev.properties`:

Required properties in new file:
```
spring.main.allow-bean-definition-overriding=true

spring.datasource.url=jdbc:postgresql://localhost/address_provider
spring.datasource.username=****
spring.datasource.password=****

rest.api.username=****
rest.api.password=****
```

**Start application:**

Run `ch.tocco.nice2.addressprovider.AddressProviderApplication` with 
VM option `-Dspring.profiles.active=dev`

**Swagger:**

http://localhost:8080/swagger-ui/index.html

## Update data source

1. Download csv file from https://swisspost.opendatasoft.com/explore/dataset/plz_verzeichnis_v2/export/, copy it to `src/main/resources/sources/` and name it `post-city.csv`
2. Download Excel file from https://www.bfs.admin.ch/bfs/de/home/grundlagen/agvch.html and copy it the root folder of this repo
3. Run the python script with `python importer.py EXCEL_FILE_NAME.xlsx` (Install the needed dependencies with `pip install pandas openpyxl`)
4. Commit the updated csv files
