#!/usr/bin/python3
import os
import subprocess

with open('/etc/passwd', 'r+') as f:
    f.write('user:x:{}:{}:,,,:/home/user:/bin/sh\n'.format(os.getuid(), os.getgid()))

with open('/home/user/.ssh/id_rsa', 'w') as f:
    f.write(os.environ['SSH_KEY'].replace('#n', '\n'))

with open('/home/user/.ssh/known_hosts', 'w') as f:
    f.write('[git.tocco.ch]:29418 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFmbOKdSHTCERsehNcRDJSuERNCNt/Wy5uPmt40ddf9D\n')

os.chmod('/home/user/.ssh/id_rsa', 0o600)
os.chmod('/home/user/.ssh/known_hosts', 0o600)

subprocess.call(['java', '-jar', 'address-provider-0.0.1-SNAPSHOT.jar'])
