#!/usr/bin/python3

import pandas as pd
import sys

if len(sys.argv) < 2:
    raise AssertionError('Pass excel file name as argument')

excelFile = sys.argv[1]

dfDistrict = pd.read_excel(excelFile, 'BZN')
dfDistrict = dfDistrict.loc[:, ~dfDistrict.columns.str.contains('^Unnamed')]
dfDistrict.to_csv('src/main/resources/sources/bfs-district.csv', index=False, sep=';')

dfCity = pd.read_excel(excelFile, 'GDE')
dfCity = dfCity.loc[:, ~dfCity.columns.str.contains('^Unnamed')]
dfCity.to_csv('src/main/resources/sources/bfs-city.csv', index=False, sep=';')
