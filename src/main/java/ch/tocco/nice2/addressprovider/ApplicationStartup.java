package ch.tocco.nice2.addressprovider;

import ch.tocco.nice2.addressprovider.db.DatabaseImporter;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Profile("!test")
public class ApplicationStartup {

    private final DatabaseImporter databaseImporter;

    public ApplicationStartup(DatabaseImporter databaseImporter) {
        this.databaseImporter = databaseImporter;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void startup() throws IOException {
        databaseImporter.importData();
    }
}
