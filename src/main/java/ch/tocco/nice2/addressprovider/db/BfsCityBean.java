package ch.tocco.nice2.addressprovider.db;

import com.opencsv.bean.CsvBindByName;

public class BfsCityBean {

    @CsvBindByName(column = "GDEBZNR", required = true)
    private int districtNr;

    @CsvBindByName(column = "GDENR", required = true)
    private int bfsNr;

    public int getDistrictNr() {
        return districtNr;
    }

    public int getBfsNr() {
        return bfsNr;
    }
}
