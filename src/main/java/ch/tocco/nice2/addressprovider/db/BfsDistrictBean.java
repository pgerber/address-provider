package ch.tocco.nice2.addressprovider.db;

import com.opencsv.bean.CsvBindByName;

public class BfsDistrictBean {

    @CsvBindByName(column = "GDEBZNR", required = true)
    private int nr;

    @CsvBindByName(column = "GDEBZNA", required = true)
    private String name;

    public int getNr() {
        return nr;
    }

    public String getName() {
        return name;
    }
}
