package ch.tocco.nice2.addressprovider.db;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class DatabaseImporter {

    private static final List<String> CANTONS = List.of("AG", "AI", "AR", "BE", "BL", "BS", "FR", "GE", "GL", "GR", "JU", "LU", "NE", "NW", "OW", "SG", "SH", "SO", "SZ", "TG", "TI", "UR", "VD", "VS", "ZG", "ZH");

    private final Logger logger = LoggerFactory.getLogger(DatabaseImporter.class);

    private final JdbcTemplate jdbcTemplate;

    @Value("${address-provider.database.allowed-zip-types}")
    private List<Integer> allowedZipTypes;

    @Value("${address-provider.source.bfs-district}")
    private String bfsDistrictFile;

    @Value("${address-provider.source.bfs-city}")
    private String bfsCityFile;

    @Value("${address-provider.source.post-city}")
    private String postCityFile;

    public DatabaseImporter(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void importData() throws IOException {
        long startTime = System.currentTimeMillis();

        List<BfsDistrictBean> bfsDistricts = readCsvFile(bfsDistrictFile, BfsDistrictBean.class);
        List<BfsCityBean> bfsCities = readCsvFile(bfsCityFile, BfsCityBean.class);
        List<PostCityBean> postCities = readCsvFile(postCityFile, PostCityBean.class);
        postCities = filterPostCities(postCities);

        Timestamp currentTimestamp = getCurrentTimestamp();
        updateDistricts(bfsDistricts);
        updateCities(bfsCities, postCities);
        deleteOldEntries(currentTimestamp);

        long endTime = System.currentTimeMillis();
        logger.info(String.format("Successfully imported in %d ms", endTime - startTime));
    }

    private <T> List<T> readCsvFile(String fileName, Class<T> clazz) throws IOException {
        try (Reader br = new InputStreamReader(new ClassPathResource(fileName).getInputStream())) {
            HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(clazz);

            CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(br)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withIgnoreEmptyLine(true)
                    .withSeparator(';')
                    .build();

            return csvToBean.parse();
        }
    }

    private List<PostCityBean> filterPostCities(List<PostCityBean> postCities) {
        return postCities.stream()
                .filter(city -> allowedZipTypes.contains(city.getZipType()))
                .filter(city -> getIsoCode(city.getCanton()) != null)
                .distinct()
                .collect(Collectors.toList());
    }

    private void updateDistricts(List<BfsDistrictBean> bfsDistricts) {
        List<Integer> existingDistrictNrs = jdbcTemplate.queryForList("SELECT nr FROM district;", Integer.class);

        List<BfsDistrictBean> bfsDistrictsUpdate = bfsDistricts.stream()
                .filter(d -> existingDistrictNrs.contains(d.getNr()))
                .collect(Collectors.toList());
        List<BfsDistrictBean> bfsDistrictsInsert = bfsDistricts.stream()
                .filter(d -> !bfsDistrictsUpdate.contains(d))
                .collect(Collectors.toList());

        String sqlInsert = "INSERT INTO district (name, create_timestamp, update_timestamp, nr) " +
                "VALUES (?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?);";
        String sqlUpdate = "UPDATE district SET name = ?, update_timestamp = CURRENT_TIMESTAMP WHERE nr = ?;";

        jdbcTemplate.batchUpdate(sqlInsert, prepareDistrict(bfsDistrictsInsert));
        jdbcTemplate.batchUpdate(sqlUpdate, prepareDistrict(bfsDistrictsUpdate));
    }

    private void updateCities(List<BfsCityBean> bfsCities, List<PostCityBean> postCities) {
        List<Pair<String, String>> existingZips = jdbcTemplate.query("SELECT zip, zip_additional_nr FROM city",
                (ResultSet rs, int rowNum) -> Pair.of(rs.getString("zip"), rs.getString("zip_additional_nr")));

        List<PostCityBean> postCitiesUpdate = postCities.stream()
                .filter(c -> existingZips.contains(Pair.of(c.getZip(), c.getZipAdditionalNr())))
                .collect(Collectors.toList());
        List<PostCityBean> postCitiesInsert = postCities.stream()
                .filter(d -> !postCitiesUpdate.contains(d))
                .collect(Collectors.toList());

        String sqlInsert = "INSERT INTO city (iso_code, name, canton, district_nr, bfs_nr, create_timestamp, " +
                "update_timestamp, zip, zip_additional_nr) " +
                "VALUES(?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?, ?);";
        String sqlUpdate = "UPDATE city SET iso_code = ?, name = ?, canton = ?, district_nr = ?, bfs_nr = ?," +
                "update_timestamp = CURRENT_TIMESTAMP WHERE zip = ? AND zip_additional_nr = ?;";

        jdbcTemplate.batchUpdate(sqlInsert, prepareCity(bfsCities, postCitiesInsert));
        jdbcTemplate.batchUpdate(sqlUpdate, prepareCity(bfsCities, postCitiesUpdate));
    }

    private Timestamp getCurrentTimestamp() {
        return jdbcTemplate.queryForObject("SELECT CURRENT_TIMESTAMP;", Timestamp.class);
    }

    private void deleteOldEntries(Timestamp timestamp) {
        jdbcTemplate.update("DELETE FROM city WHERE update_timestamp < ?;", new Object[]{timestamp}, new int[]{Types.TIMESTAMP});
        jdbcTemplate.update("DELETE FROM district WHERE update_timestamp < ?;", new Object[]{timestamp}, new int[]{Types.TIMESTAMP});
    }

    private String getIsoCode(String canton) {
        if ("FL".equals(canton)) {
            return "FL";
        } else if (CANTONS.contains(canton)) {
            return "CH";
        } else {
            // special case for Campione d'Italia and Büsingen which have suisse zip codes
            return null;
        }
    }

    private BatchPreparedStatementSetter prepareDistrict(List<BfsDistrictBean> bfsDistricts) {
        return new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                BfsDistrictBean bean = bfsDistricts.get(i);
                ps.setString(1, bean.getName());
                ps.setInt(2, bean.getNr());
            }

            @Override
            public int getBatchSize() {
                return bfsDistricts.size();
            }
        };
    }

    private BatchPreparedStatementSetter prepareCity(List<BfsCityBean> bfsCities, List<PostCityBean> postCities) {
        return new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PostCityBean postBean = postCities.get(i);
                Optional<BfsCityBean> bfsBean = bfsCities.stream().filter(city -> city.getBfsNr() == postBean.getBfsNr()).findFirst();

                ps.setString(1, getIsoCode(postBean.getCanton()));
                ps.setString(2, postBean.getName());
                ps.setString(3, postBean.getCanton());
                if (bfsBean.isPresent()) {
                    ps.setInt(4, bfsBean.get().getDistrictNr());
                } else {
                    ps.setNull(4, java.sql.Types.INTEGER);
                }
                ps.setInt(5, postBean.getBfsNr());
                ps.setString(6, postBean.getZip());
                ps.setString(7, postBean.getZipAdditionalNr());
            }

            @Override
            public int getBatchSize() {
                return postCities.size();
            }
        };
    }
}
