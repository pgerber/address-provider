package ch.tocco.nice2.addressprovider.db;

import com.opencsv.bean.CsvBindByName;

import java.util.Objects;

public class PostCityBean {

    @CsvBindByName(column = "BFSNR", required = true)
    private int bfsNr;

    @CsvBindByName(column = "PLZ_TYP", required = true)
    private int zipType;

    @CsvBindByName(column = "POSTLEITZAHL", required = true)
    private String zip;

    @CsvBindByName(column = "PLZ_ZZ", required = true)
    private String zipAdditionalNr;

    @CsvBindByName(column = "ORTBEZ27", required = true)
    private String name;

    @CsvBindByName(column = "KANTON", required = true)
    private String canton;

    public int getBfsNr() {
        return bfsNr;
    }

    public int getZipType() {
        return zipType;
    }

    public String getZip() {
        return zip;
    }

    public String getZipAdditionalNr() {
        return zipAdditionalNr;
    }

    public String getName() {
        return name;
    }

    public String getCanton() {
        return canton;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostCityBean that = (PostCityBean) o;
        return bfsNr == that.bfsNr &&
                zipType == that.zipType &&
                Objects.equals(zip, that.zip) &&
                Objects.equals(zipAdditionalNr, that.zipAdditionalNr) &&
                Objects.equals(name, that.name) &&
                Objects.equals(canton, that.canton);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bfsNr, zipType, zip, zipAdditionalNr, name, canton);
    }
}
