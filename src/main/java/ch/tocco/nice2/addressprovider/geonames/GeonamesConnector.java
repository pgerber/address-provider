package ch.tocco.nice2.addressprovider.geonames;

import ch.tocco.nice2.addressprovider.rest.api.Result;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
public class GeonamesConnector {

    private final RestTemplate restTemplate;

    @Value("${geonames.api.url}")
    private String url;

    @Value("${geonames.api.user}")
    private String user;

    public GeonamesConnector(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Result> search(String postcodeStartsWith, String cityStartsWith, List<String> countries, int maxRows) {
        if(maxRows == 0){
            return List.of();
        }

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url)
                .queryParam("username", user)
                .queryParam("maxRows", maxRows);

        countries.forEach(country -> uriBuilder.queryParam("country", country));

        if (!Strings.isNullOrEmpty(postcodeStartsWith)) {
            uriBuilder.queryParam("postalcode_startsWith", postcodeStartsWith);
        }

        if (!Strings.isNullOrEmpty(cityStartsWith)) {
            uriBuilder.queryParam("placename_startsWith", cityStartsWith);
        }

        GeoNamesResponseBean responseBean = restTemplate.getForObject(uriBuilder.toUriString(), GeoNamesResponseBean.class);
        List<Result> results = responseBean.toResults();

        results.sort((Result r1, Result r2) -> {
            if (r1.getCity().equals(r2.getCity())) {
                return r1.getPostcode().compareTo(r2.getPostcode());
            } else {
                return r1.getCity().compareTo(r2.getCity());
            }
        });

        return results;
    }
}
