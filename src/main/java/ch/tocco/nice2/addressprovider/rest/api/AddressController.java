package ch.tocco.nice2.addressprovider.rest.api;

import ch.tocco.nice2.addressprovider.db.DatabaseConnector;
import ch.tocco.nice2.addressprovider.geonames.GeonamesConnector;
import ch.tocco.nice2.addressprovider.rest.error.BadRequestException;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "rest/v1/locations")
@ApiResponses(value = {
        @ApiResponse(code = 401, message = "Unauthorized: Use http basic authentication"),
        @ApiResponse(code = 500, message = "Internal Server Error")
})
@Api(tags = "Address Controller")
public class AddressController {

    private final GeonamesConnector geonamesConnector;

    private final DatabaseConnector databaseConnector;

    @Value("${address-provider.database.supported-countries}")
    private List<String> dbSupportedCountries;

    @Value("${address-provider.limit}")
    private int limit;

    public AddressController(GeonamesConnector geonamesConnector, DatabaseConnector databaseConnector) {
        this.geonamesConnector = geonamesConnector;
        this.databaseConnector = databaseConnector;
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET, produces = {"application/json"})
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad Request: Either postcodeStartsWith or cityStartsWith must be set and at least one country must be passed")
    })
    @Operation(
            summary = "Search locations",
            description = "Search locations with a name and/or postcode in a list of countries"
    )
    public ResultBean search(@ApiParam(value = "first characters of postcode") @RequestParam(required = false) String postcodeStartsWith,
                             @ApiParam(value = "first characters of city name") @RequestParam(required = false) String cityStartsWith,
                             @ApiParam(value = "country iso codes, the country parameter may occur more than once, e.g. country=DE&country=LI&country=CH", allowMultiple=true) @RequestParam(required = false, name = "country") List<String> countries) {

        if (Strings.isNullOrEmpty(postcodeStartsWith) && Strings.isNullOrEmpty(cityStartsWith)) {
            throw new BadRequestException("Either postcodeStartsWith or cityStartsWith must be set");
        }

        postcodeStartsWith = Strings.nullToEmpty(postcodeStartsWith);
        cityStartsWith = Strings.nullToEmpty(cityStartsWith);

        if (countries == null || countries.isEmpty()) {
            throw new BadRequestException("At least one country must be passed as query parameter");
        }

        List<String> countriesDb = countries.stream().filter(dbSupportedCountries::contains).collect(Collectors.toList());
        List<String> countriesGeonames = countries.stream().filter(c -> !countriesDb.contains(c)).collect(Collectors.toList());

        List<Result> results = Lists.newArrayList();
        if (!countriesDb.isEmpty()) {
            results.addAll(databaseConnector.search(postcodeStartsWith, cityStartsWith, countriesDb));
        }

        if (!countriesGeonames.isEmpty()) {
            results.addAll(geonamesConnector.search(postcodeStartsWith, cityStartsWith, countriesGeonames, limit - results.size()));
        }

        return new ResultBean(results);
    }

    @Operation(
            summary = "Load all locations from the database",
            description = "Load all locations from the database"
    )
    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
    public ResultBean getAll() {
        return new ResultBean(databaseConnector.getAll());
    }
}
