package ch.tocco.nice2.addressprovider.rest.api;

import java.util.Objects;

public class Result {

    private String city;
    private String postcode;
    private String state;
    private String district;
    private String country;
    private String postcodeAdditionalNr;

    public Result() {
    }

    public Result(String city, String postcode, String state, String district, String country, String postcodeAdditionalNr) {
        this.city = city;
        this.postcode = postcode;
        this.state = state;
        this.district = district;
        this.country = country;
        this.postcodeAdditionalNr = postcodeAdditionalNr;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getState() {
        return state;
    }

    public String getDistrict() {
        return district;
    }

    public String getCountry() {
        return country;
    }

    public String getPostcodeAdditionalNr() {
        return postcodeAdditionalNr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result result = (Result) o;
        return Objects.equals(city, result.city) &&
                Objects.equals(postcode, result.postcode) &&
                Objects.equals(state, result.state) &&
                Objects.equals(district, result.district) &&
                Objects.equals(country, result.country) &&
                Objects.equals(postcodeAdditionalNr, result.postcodeAdditionalNr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, postcode, state, district, country, postcodeAdditionalNr);
    }
}
