package ch.tocco.nice2.addressprovider;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class AbstractDatabaseTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void resetDatabase() throws IOException {
        clearDatabase();
        loadData();
    }

    public void clearDatabase() {
        jdbcTemplate.execute("DELETE FROM city;");
        jdbcTemplate.execute("DELETE FROM district;");
    }

    private void loadData() throws IOException {
        InputStream inputStream = new ClassPathResource("data.sql").getInputStream();
        String text = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));

        jdbcTemplate.execute(text);
    }
}
