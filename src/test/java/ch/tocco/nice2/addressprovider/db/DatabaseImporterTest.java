package ch.tocco.nice2.addressprovider.db;

import ch.tocco.nice2.addressprovider.AbstractDatabaseTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@SpringBootTest
@ActiveProfiles("test")
public class DatabaseImporterTest extends AbstractDatabaseTest {

    @Autowired
    private DatabaseImporter databaseImporter;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${address-provider.source.bfs-district}")
    private String defaultBfsDistrictFile;

    @Value("${address-provider.source.bfs-city}")
    private String defaultBfsCityFile;

    @Value("${address-provider.source.post-city}")
    private String defaultPostCityFile;

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(databaseImporter, "bfsDistrictFile", defaultBfsDistrictFile);
        ReflectionTestUtils.setField(databaseImporter, "bfsCityFile", defaultBfsCityFile);
        ReflectionTestUtils.setField(databaseImporter, "postCityFile", defaultPostCityFile);
    }

    @Test
    public void importDataSuccess() throws IOException {
        databaseImporter.importData();

        assertEquals(2, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM district;", Integer.class));

        assertEquals(7, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city;", Integer.class));
        List<String> cities = jdbcTemplate.queryForList("SELECT name FROM city;", String.class);
        List<String> zips = jdbcTemplate.queryForList("SELECT zip FROM city;", String.class);
        List.of("Stäfa", "Feldbach", "Gockhausen", "Zürich").forEach(name ->
                assertTrue(cities.contains(name))
        );
        List.of("8712", "8714", "8000", "8032", "8047", "8044").forEach(zip ->
                assertTrue(zips.contains(zip))
        );
        // "Zürich Briefzentrum", "Zürich Helvetiaplatz" & "Stäfa Zustellung" -> zip type not 10 or 20
        // "Campione d'Italia" & "Büsingen" -> skipped because not CH or LI
        List.of("Zürich Briefzentrum", "Stäfa Zustellung", "Zürich Helvetiaplatz", "Campione d'Italia", "Büsingen").forEach(name ->
                assertFalse(cities.contains(name))
        );
        List.of("8022", "8026", "8010", "6911", "8238").forEach(zip ->
                assertFalse(zips.contains(zip))
        );
        assertEquals(1, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM (SELECT zip, COUNT(*) FROM city GROUP BY zip HAVING COUNT(*) > 1);", Integer.class));
        assertEquals(2, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city WHERE zip = '8044';", Integer.class));
        assertEquals(7, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city WHERE iso_code = 'CH';", Integer.class));
        assertEquals(7, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city WHERE canton = 'ZH';", Integer.class));

        // "Feldbach" & "Gockhausen" -> does not exists in the bfs data set
        assertEquals(List.of(153, 191), jdbcTemplate.queryForList("SELECT bfs_nr FROM city WHERE district_nr IS NULL;", Integer.class));

        // "Gockhausen" has an additional zip number
        assertEquals(List.of(191), jdbcTemplate.queryForList("SELECT bfs_nr FROM city WHERE zip_additional_nr <> '00';", Integer.class));
    }

    @ParameterizedTest
    @MethodSource("importDataFailedArguments")
    public void importDataFailed(String bfsDistrictFile, String bfsCityFile, String postCityFile, String errorMsg1, String errorMsg2) {
        if (bfsDistrictFile != null) {
            ReflectionTestUtils.setField(databaseImporter, "bfsDistrictFile", bfsDistrictFile);
        }
        if (bfsCityFile != null) {
            ReflectionTestUtils.setField(databaseImporter, "bfsCityFile", bfsCityFile);
        }
        if (postCityFile != null) {
            ReflectionTestUtils.setField(databaseImporter, "postCityFile", postCityFile);
        }

        Exception exception = assertThrows(RuntimeException.class, () -> databaseImporter.importData());

        assertEquals(errorMsg1, exception.getMessage());
        assertEquals(errorMsg2, exception.getCause().getMessage());

        assertEquals(2, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM district;", Integer.class));
        assertEquals(3, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city;", Integer.class));
    }

    private static Stream<Arguments> importDataFailedArguments() {
        return Stream.of(
                // column missing
                arguments("sources/bfs-district_missing_column.csv", null, null,
                        "Error capturing CSV header!",
                        "Header is missing required fields [GDEBZNA]. The list of headers encountered is [GDEKT,GDEBZNR]."),
                arguments(null, "sources/bfs-city_missing_column.csv", null,
                        "Error capturing CSV header!",
                        "Header is missing required fields [GDEBZNR]. The list of headers encountered is [GDEKT,GDENR,GDENAME,GDENAMK,GDEBZNA,GDEKTNA,GDEMUTDAT]."),
                arguments(null, null, "sources/post-city_missing_column.csv",
                        "Error capturing CSV header!",
                        "Header is missing required fields [POSTLEITZAHL]. The list of headers encountered is [REC_ART,ONRP,BFSNR,PLZ_TYP,PLZ_ZZ,GPLZ,ORTBEZ18,ORTBEZ27,KANTON,SPRACHCODE,SPRACHCODE_ ABW,BRIEFZ_DURCH,GILT_AB_DAT,PLZ_BRIEFZUST,PLZ_COFF,Geo Shape,Geokoordinaten]."),

                // wrong separator
                arguments("sources/bfs-district_wrong_separator.csv", null, null,
                        "Error capturing CSV header!",
                        "Header is missing required fields [GDEBZNA, GDEBZNR]. The list of headers encountered is [GDEKT   GDEBZNR GDEBZNA]."),
                arguments(null, "sources/bfs-city_wrong_separator.csv", null,
                        "Error capturing CSV header!",
                        "Header is missing required fields [GDEBZNR, GDENR]. The list of headers encountered is [GDEKT,GDEBZNR,GDENR,GDENAME,GDENAMK,GDEBZNA,GDEKTNA,GDEMUTDAT]."),
                arguments(null, null, "sources/post-city_wrong_separator.csv",
                        "Error capturing CSV header!",
                        "Header is missing required fields [BFSNR, KANTON, ORTBEZ27, PLZ_TYP, PLZ_ZZ, POSTLEITZAHL]. The list of headers encountered is [REC_ART,ONRP,BFSNR,PLZ_TYP,POSTLEITZAHL,PLZ_ZZ,GPLZ,ORTBEZ18,ORTBEZ27,KANTON,SPRACHCODE,SPRACHCODE_ ABW,BRIEFZ_DURCH,GILT_AB_DAT,PLZ_BRIEFZUST,PLZ_COFF,Geo Shape,Geokoordinaten]."),

                // empty file
                arguments("sources/empty.csv", null, null,
                        "Error capturing CSV header!",
                        "Header is missing required fields [GDEBZNA, GDEBZNR]. The list of headers encountered is []."),
                arguments(null, "sources/empty.csv", null,
                        "Error capturing CSV header!",
                        "Header is missing required fields [GDEBZNR, GDENR]. The list of headers encountered is []."),
                arguments(null, null, "sources/empty.csv",
                        "Error capturing CSV header!",
                        "Header is missing required fields [BFSNR, KANTON, ORTBEZ27, PLZ_TYP, PLZ_ZZ, POSTLEITZAHL]. The list of headers encountered is [].")
        );
    }

    @Test
    public void emptyLines() throws IOException {
        ReflectionTestUtils.setField(databaseImporter, "bfsDistrictFile", "sources/bfs-district_empty_lines.csv");
        ReflectionTestUtils.setField(databaseImporter, "bfsCityFile", "sources/bfs-city_empty_lines.csv");
        ReflectionTestUtils.setField(databaseImporter, "postCityFile", "sources/post-city_empty_lines.csv");
        databaseImporter.importData();

        assertEquals(2, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM district;", Integer.class));
        assertEquals(7, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city;", Integer.class));
    }

    @Test
    public void differentColumns() throws IOException {
        ReflectionTestUtils.setField(databaseImporter, "bfsDistrictFile", "sources/bfs-district_different_columns.csv");
        ReflectionTestUtils.setField(databaseImporter, "bfsCityFile", "sources/bfs-city_different_columns.csv");
        ReflectionTestUtils.setField(databaseImporter, "postCityFile", "sources/post-city_different_columns.csv");
        databaseImporter.importData();

        assertEquals(2, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM district;", Integer.class));
        assertEquals(7, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city;", Integer.class));
    }

    @Test
    public void invalidFile() {
        clearDatabase();
        ReflectionTestUtils.setField(databaseImporter, "bfsDistrictFile", "sources/invalid_file.csv");

        Exception exception = assertThrows(FileNotFoundException.class, () -> databaseImporter.importData());

        assertEquals("class path resource [sources/invalid_file.csv] cannot be opened because it does not exist", exception.getMessage());

        assertEquals(0, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM district;", Integer.class));
        assertEquals(0, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city;", Integer.class));
    }

    @Test
    public void updateDatabaseValues() throws IOException {
        ReflectionTestUtils.setField(databaseImporter, "bfsDistrictFile", "sources/bfs-district_update.csv");
        ReflectionTestUtils.setField(databaseImporter, "bfsCityFile", "sources/bfs-city_update.csv");
        ReflectionTestUtils.setField(databaseImporter, "postCityFile", "sources/post-city_update.csv");

        databaseImporter.importData();

        assertEquals(2, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM district;", Integer.class));
        // district Zürich dropped
        assertEquals(0, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM district WHERE nr = 112;", Integer.class));
        // new district Horgen
        assertEquals("Bezirk Horgen", jdbcTemplate.queryForObject("SELECT name FROM district WHERE nr = 106;", String.class));
        // district Meilen renamed to Pfannenstiel
        assertEquals(0, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM district WHERE name LIKE 'Bezrik Meilen';", Integer.class));
        assertEquals("Bezirk Pfannelstiel", jdbcTemplate.queryForObject("SELECT name FROM district WHERE nr = 107;", String.class));

        // city Zürich dropped
        assertEquals(0, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city WHERE name = 'Zürich';", Integer.class));
        // update city Stäfa (change name, bfs nr)
        assertEquals(0, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city WHERE bfs_nr = 158;", Integer.class));
        assertEquals("Politische Gemeinde Stäfa", jdbcTemplate.queryForObject("SELECT name FROM city WHERE bfs_nr = 9999 AND zip <> 8713;", String.class));
        // update city Männedorf (change plz)
        assertEquals(0, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city WHERE zip = 8708;", Integer.class));
        assertEquals(1, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city WHERE zip = 9999;", Integer.class));
        // new city Uerikon
        assertEquals(1, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM city WHERE name = 'Uerikon';", Integer.class));
    }
}
