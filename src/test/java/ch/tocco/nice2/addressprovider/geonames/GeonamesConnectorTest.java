package ch.tocco.nice2.addressprovider.geonames;

import ch.tocco.nice2.addressprovider.rest.api.Result;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@SpringBootTest
@ActiveProfiles("test")
public class GeonamesConnectorTest {

    @Autowired
    private GeonamesConnector geonamesConnector;

    @Autowired
    private RestTemplate restTemplate;

    @Value("classpath:geonames.json")
    private Resource jsonResponse;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @ParameterizedTest
    @MethodSource("searchData")
    public void search(String zipStartsWith, String cityStartsWith, List<String> countries, String uri) throws IOException {
        mockRequest(uri);
        List<Result> list = geonamesConnector.search(zipStartsWith, cityStartsWith, countries, 10);
        assertThat(list, hasSize(5));

        mockServer.verify();
    }

    private static Stream<Arguments> searchData() {
        return Stream.of(
                arguments("8", "", List.of("AT"), "http://api.geonames.org/postalCodeSearchJSON?username=test&maxRows=10&country=AT&postalcode_startsWith=8"),
                arguments("", "Graz", List.of("AT"), "http://api.geonames.org/postalCodeSearchJSON?username=test&maxRows=10&country=AT&placename_startsWith=Graz"),
                arguments("", "Graz", List.of("AT", "DE"), "http://api.geonames.org/postalCodeSearchJSON?username=test&maxRows=10&country=AT&country=DE&placename_startsWith=Graz")
        );
    }

    @Test
    public void searchMaxRowsZero() {
        List<Result> list = geonamesConnector.search("", "", List.of(), 0);
        assertThat(list, hasSize(0));
    }

    @Test
    public void sorting() throws IOException {
        mockRequest("http://api.geonames.org/postalCodeSearchJSON?username=test&maxRows=10&country=AT&postalcode_startsWith=8063");
        List<Result> list = geonamesConnector.search("8063", "", List.of("AT"), 10);

        assertThat(list, hasSize(5));
        List<String> expected = List.of("Edelsbach bei Graz", "Eggersdorf bei Graz", "Haselbach", "Höf", "Purgstall bei Eggersdorf");
        assertEquals(expected, list.stream().map(Result::getCity).collect(Collectors.toList()));
    }

    private void mockRequest(String uri) throws IOException {
        String response = StreamUtils.copyToString(jsonResponse.getInputStream(), Charset.forName("UTF-8"));
        mockServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));
    }
}
