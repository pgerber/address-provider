package ch.tocco.nice2.addressprovider.rest.api;

import ch.tocco.nice2.addressprovider.AbstractDatabaseTest;
import ch.tocco.nice2.addressprovider.db.DatabaseConnector;
import ch.tocco.nice2.addressprovider.geonames.GeonamesConnector;
import ch.tocco.nice2.addressprovider.rest.error.ExceptionResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

import static ch.tocco.nice2.addressprovider.rest.api.ResultMatcher.isResult;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AddressControllerTest extends AbstractDatabaseTest {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restClient;

    @MockBean
    private GeonamesConnector geonamesConnector;

    @MockBean
    private DatabaseConnector databaseConnector;

    private final Result resultDatabase1 = new Result("Zürich", "8000", "ZH", "Bezirk Zürich", "CH", "00");
    private final Result resultDatabase2 = new Result("Bern", "3000", "BE", "Verwaltungskreis Bern-Mittelland", "CH", "00");
    private final Result resultGeonames = new Result("Eggersdorf bei Graz", "8063", "06", "Politischer Bezirk Graz-Umgebung", "AT", "00");

    @Test
    public void searchDatabase() {
        when(databaseConnector.search(anyString(), anyString(), anyList())).thenReturn(List.of(resultDatabase1));

        List<Result> list = restClient.getForObject(String.format("http://localhost:%s/rest/v1/locations/search?country=CH&postcodeStartsWith=8", port), ResultBean.class).getResults();
        assertThat(list, hasSize(1));
        assertThat(list, contains(isResult(resultDatabase1)));
    }

    @Test
    public void searchGeonames() {
        when(geonamesConnector.search(anyString(), anyString(), anyList(), anyInt())).thenReturn(List.of(resultGeonames));

        List<Result> list = restClient.getForObject(String.format("http://localhost:%s/rest/v1/locations/search?country=AT&postcodeStartsWith=8", port), ResultBean.class).getResults();
        assertThat(list, hasSize(1));
        assertThat(list, contains(isResult(resultGeonames)));
    }

    @Test
    public void search() {
        when(databaseConnector.search(anyString(), anyString(), anyList())).thenReturn(List.of(resultDatabase1));
        when(geonamesConnector.search(anyString(), anyString(), anyList(), anyInt())).thenReturn(List.of(resultGeonames));

        List<Result> list = restClient.getForObject(String.format("http://localhost:%s/rest/v1/locations/search?country=CH&country=AT&postcodeStartsWith=8", port), ResultBean.class).getResults();
        assertEquals(List.of(resultDatabase1, resultGeonames), list);
    }

    @Test
    public void missingSearchString() {
        ResponseEntity<ExceptionResponse> response = restClient.exchange(String.format("http://localhost:%s/rest/v1/locations/search?country=CH", port), HttpMethod.GET, null, ExceptionResponse.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Either postcodeStartsWith or cityStartsWith must be set", response.getBody().getMessage());
    }

    @Test
    public void missingCountry() {
        ResponseEntity<ExceptionResponse> response = restClient.exchange(String.format("http://localhost:%s/rest/v1/locations/search?postcodeStartsWith=8", port), HttpMethod.GET, null, ExceptionResponse.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("At least one country must be passed as query parameter", response.getBody().getMessage());
    }

    @Test
    public void getAll() {
        when(databaseConnector.getAll()).thenReturn(List.of(resultDatabase2, resultDatabase1));

        List<Result> list = restClient.getForObject(String.format("http://localhost:%s/rest/v1/locations", port), ResultBean.class).getResults();
        assertThat(list, hasSize(2));
        assertEquals(List.of(resultDatabase2, resultDatabase1), list);
    }

    @Test
    public void unauthorized() {
        RestTemplate restClient = new RestTemplateBuilder()
                .errorHandler(new RestResponseErrorHandler())
                .build();

        ResponseEntity<ResultBean> response = restClient.exchange(String.format("http://localhost:%s/rest/v1/locations/", port), HttpMethod.GET, null, ResultBean.class);
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @TestConfiguration
    static class RestTestConfiguration {

        @Value("${rest.api.username}")
        private String username;

        @Value("${rest.api.password}")
        private String password;

        @Bean("restClient")
        public RestTemplate restClient() {
            return new RestTemplateBuilder()
                    .basicAuthentication(username, password)
                    .errorHandler(new RestResponseErrorHandler())
                    .build();
        }
    }

    @Component
    static class RestResponseErrorHandler implements ResponseErrorHandler {

        @Override
        public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
            return httpResponse.getStatusCode().series() == CLIENT_ERROR || httpResponse.getStatusCode().series() == SERVER_ERROR;
        }

        @Override
        public void handleError(ClientHttpResponse httpResponse) {

        }
    }
}
