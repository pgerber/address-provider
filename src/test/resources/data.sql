MERGE INTO district (nr, name, create_timestamp, update_timestamp) VALUES(107, 'Bezirk Meilen', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
MERGE INTO district (nr, name, create_timestamp, update_timestamp) VALUES(112, 'Bezirk Zürich', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

MERGE INTO city (iso_code, zip, zip_additional_nr, name, canton, district_nr, bfs_nr, create_timestamp, update_timestamp) KEY(zip, zip_additional_nr) VALUES('CH', '8712', '00', 'Stäfa', 'ZH', 107, 158, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
MERGE INTO city (iso_code, zip, zip_additional_nr, name, canton, district_nr, bfs_nr, create_timestamp, update_timestamp) KEY(zip, zip_additional_nr) VALUES('CH', '8708', '00', 'Männedorf', 'ZH', null, 155, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
MERGE INTO city (iso_code, zip, zip_additional_nr, name, canton, district_nr, bfs_nr, create_timestamp, update_timestamp) KEY(zip, zip_additional_nr) VALUES('CH', '8000', '00', 'Zürich', 'ZH', 112, 261, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
